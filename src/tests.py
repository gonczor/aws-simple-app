import json
import unittest

from sqlalchemy_utils import database_exists, create_database, drop_database

from main import app, db
from config import DB_HOST, DB_PASSWORD, DB_USERNAME
from models import Visitor, Visit

DB_NAME = 'test_db'


class test_db:
    def __enter__(self):
        self.database_uri = f'postgresql://{DB_USERNAME}:{DB_PASSWORD}@{DB_HOST}/test_{DB_NAME}'
        if database_exists(self.database_uri):
            drop_database(self.database_uri)
        create_database(self.database_uri)
        return self

    def __exit__(self, *args):
        drop_database(self.database_uri)


class TestViews(unittest.TestCase):
    def setUp(self) -> None:
        app.config['TESTING'] = True
        SQLALCHEMY_DATABASE_URI = \
            f'postgresql://{DB_USERNAME}:{DB_PASSWORD}@{DB_HOST}/test_{DB_NAME}'
        app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
        app.app_context().push()
        self.client = app.test_client()
        db.session.commit()
        db.drop_all()
        db.create_all()

    def test_main_page_without_visitors(self):
        response = self.client.get('/')
        self.assertEqual(json.loads(response.data), {'results': []})

    def test_main_page_with_visitors(self):
        visitor_username = 'visitor'
        visitor = Visitor(username=visitor_username)
        db.session.add(visitor)
        db.session.commit()
        visitor_id = visitor.ID
        visit = Visit(visitor=visitor_id)
        db.session.add(visit)
        db.session.commit()

        response = self.client.get('/')
        self.assertEqual(
            json.loads(response.data),
            {
                'results': [{'id': visitor_id, 'username': visitor_username, 'visits': 1}]
            }
        )

    def test_increment_visits_creates_new_visitor(self):
        self.client.get('/new visitor/')

        visitor = Visitor.query.filter_by(username='new visitor').first()
        visits_count = Visit.query.filter_by(visitor=visitor.ID).count()
        self.assertIsNotNone(visitor)
        self.assertEqual(visitor.username, 'new visitor')
        self.assertEqual(visits_count, 1)

    def test_increment_visits_with_existing_visitor(self):
        visitor = Visitor(username='test')
        db.session.add(visitor)
        db.session.commit()
        db.session.add(Visit(visitor=visitor.ID))
        db.session.commit()

        response = json.loads(self.client.get('/test/').data)

        self.assertEqual(response['username'], 'test')
        self.assertEqual(response['visits'], 2)

    def test_favicon_is_not_saved(self):
        self.client.get('/favicon.ico/')

        visitor = Visitor.query.filter_by(username='favicon.ico').first()
        self.assertIsNone(visitor)

        response = self.client.get('/')
        self.assertEqual(json.loads(response.data), {'results': []})


if __name__ == '__main__':
    with test_db():
        unittest.main()
