from commands import increment_visits_for_visitor
from queries import get_all_visits, get_visits_for_visitor, get_visitor_by_username


def index():
    results = get_all_visits()
    return {"results": results}


def increment_visits(username: str) -> dict:
    if username == "favicon.ico":
        return {}
    visitor = get_visitor_by_username(username)
    increment_visits_for_visitor(visitor)
    return {
        "id": visitor.ID,
        "username": visitor.username,
        "visits": get_visits_for_visitor(visitor)
    }
