from models import Visitor, Visit, db


def increment_visits_for_visitor(visitor: Visitor):
    db.session.add(Visit(visitor=visitor.ID))
    db.session.commit()
