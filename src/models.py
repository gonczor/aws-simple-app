from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
migrate = Migrate()


class Visitor(db.Model):
    ID = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True)


class Visit(db.Model):
    ID = db.Column(db.Integer, primary_key=True)
    visitor = db.Column(db.Integer, db.ForeignKey('visitor.ID'), index=True)
