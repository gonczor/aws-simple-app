import sys

from flask import Flask
from flask_migrate import migrate, init, upgrade, downgrade

from config import SQLALCHEMY_DATABASE_URI, HOST, PORT, DEBUG, SQLALCHEMY_ENGINE_OPTIONS
from models import db, migrate as db_migrate


def make_app() -> Flask:
    flask_app = Flask(__name__)
    flask_app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
    flask_app.config['SQLALCHEMY_ENGINE_OPTIONS'] = SQLALCHEMY_ENGINE_OPTIONS
    add_urls(flask_app)
    db.init_app(app=flask_app)
    db_migrate.init_app(flask_app, db)
    return flask_app


def add_urls(flask_app: Flask):
    from views import index, increment_visits
    flask_app.add_url_rule('/', view_func=index)
    flask_app.add_url_rule('/<username>/', view_func=increment_visits)


app = make_app()

if __name__ == '__main__':
    if 'createdb' in sys.argv:
        app.app_context().push()
        db.create_all()
    elif 'init' in sys.argv:
        app.app_context().push()
        init()
    elif 'migrate' in sys.argv:
        try:
            message = sys.argv[2]
            app.app_context().push()
            migrate(message=message)
        except IndexError:
            print('Usage: python main.py migrate <message>', flush=True)
    elif 'upgrade' in sys.argv:
        app.app_context().push()
        upgrade()
    elif 'downgrade' in sys.argv:
        app.app_context().push()
        downgrade()
    else:
        app.run(host=HOST, port=PORT, debug=DEBUG)
