from sqlalchemy import func
from models import Visitor, db, Visit


def get_all_visits() -> list:
    visitors = db.session.query(
        Visitor.ID,
        Visitor.username,
        func.count(Visit.ID).label('visits')
    ).outerjoin(
        Visit
    ).group_by(
        Visitor.ID
    )
    db.session.close()
    response = [
        {
            'id': visitor.ID,
            'username': visitor.username,
            'visits': visitor.visits
        }
        for visitor in visitors
    ]
    return response


def get_visits_for_visitor(visitor: Visitor) -> int:
    return db.session.query(Visit).filter(Visit.visitor == visitor.ID).count()


def get_visitor_by_username(username: str) -> Visitor:
    visitor = Visitor.query.filter_by(username=username).first()
    if visitor is None:
        visitor = Visitor(username=username)
        db.session.add(visitor)
    db.session.commit()
    return visitor
